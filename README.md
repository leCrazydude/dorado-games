## Blog Challenge for Dorado Games using Vue.js V2.0
> by Maurice

**Clone Project**<br />
*Create a new empty directory and clone the project from GitLab using the following command:*<br />
```git clone https://gitlab.com/leCrazydude/dorado-games.git```

**Open Project Folder**<br />
*Use preferred editor to locate and open up the git repository named* ```dorado-games```

**Go to Project Directory using Terminal**<br />
*Open up a terminal window and traverse to* ```.\dorado-blog\```

**Install Dependencies**<br />
*Install the required dependencies using* ```npm install``` *or* ```yarn install```

**Launch the App**<br />
*Execute* ```npm start``` *in the terminal window and open up the localhost url with the specified port*